# Bud – Frontend Developer Exercise

## Install and run

```bash
# Install dependencies
yarn install

# Serve dev env at localhost:8080
yarn start

# Build static files
yarn build

# run test cases
yarn jest

# run linter
yarn lint

```
