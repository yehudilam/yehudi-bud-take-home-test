import React, { useEffect, useState } from 'react'
import styled from 'styled-components'
import TransactionItem from './components/TransactionItem'
import { Balance, Provider, Transaction } from './interfaces'
import TransactionHeader from './components/TransactionHeader'
import { mockData } from './api/mockData'

const Main = styled.main`
  & h2, h3 {
    margin: 8px 0;
    font-size: 1.8rem;
  }

  & p {
    margin: 2px 0;
  }
`

const Title = styled.h1`
  font-weight: bold;
  font-size: 2.2rem;
  margin: 12px 0;
`

const Section = styled.section`
  margin-bottom: 12px;
`

const Transactions = styled.div`
  max-width: 100%;
  overflow-x: scroll;
  padding: 0 8px 12px 8px;
`

const App = (): JSX.Element => {
  const [loading, setLoading] = useState(true)
  const [error, setError] = useState<string>('')

  const [provider, setProvider] = useState<Provider | undefined>(undefined)
  const [balance, setBalance] = useState<Balance | undefined>(undefined)
  const [transactions, setTransactions] = useState<Transaction[]>([])

  useEffect(() => {
    const fetchData = async (): Promise<void> => {
      try {
        setLoading(true)
        setError('')

        const { provider, balance, transactions } = await mockData()

        setProvider(provider)
        setBalance(balance)
        setTransactions(transactions
          .filter((tx: Transaction) => tx.category_title.toLowerCase() !== 'income')
          .sort((a: Transaction, b: Transaction) => Math.abs(a.amount.value) < Math.abs(b.amount.value) ? -1 : 1)
          .slice(0, 10))
        setLoading(false)
      } catch (e) {
        if (e instanceof Error) {
          setError(e.message)
        } else {
          setError('Unknown Error')
        }

        console.error(e)
      }
    }

    void fetchData()
  }, [])

  if (loading) {
    return <div>Loading...</div>
  }

  if (error !== '') {
    return <div>{error}</div>
  }

  return (
    <Main>

      {/*  provider  */}
      <Section>
        <Title>{provider?.title ?? ''}</Title>
        <p>Description: {provider?.description ?? ''}</p>
        <h3>Account Number: {provider?.account_number ?? ''}</h3>
      </Section>

      {/*  balance  */}
      <Section>
        <h2>Balance: {balance?.currency_iso ?? ''} {balance?.amount ?? ''}</h2>
      </Section>

      {/*  transactions  */}
      <Section>
        <Transactions>
          <TransactionHeader/>
          {transactions?.map((tx: Transaction) => (
            <TransactionItem key={tx.id} transaction={tx}/>
          ))}
        </Transactions>

        <p>Only the 10 smallest expenses are shown here</p>
      </Section>

    </Main>
  )
}

export default App
