export interface Transaction {
  id: string
  date: string
  description: string
  category_title: string
  amount: {
    value: number
    currency_iso: string
  }
}

export interface Provider {
  title: string
  account_number: string
  sort_code: string
  description: string
}

export interface Balance {
  currency_iso: string
  amount: number
}

export interface MockApiResponse {
  provider: Provider
  balance: Balance
  transactions: Transaction[]
}
