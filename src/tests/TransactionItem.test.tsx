import * as React from 'react'
import TransactionItem from '../components/TransactionItem'
import { render, screen } from '@testing-library/react'
import '@testing-library/jest-dom'

test('renders transaction item', () => {
  const transaction = {
    id: 'id1',
    date: '2022-09-01',
    description: 'test transaction',
    category_title: 'transaction',
    amount: {
      value: -100.01,
      currency_iso: 'GBP'
    }
  }

  render(<TransactionItem transaction={transaction} />)

  const dateEl = screen.getByText(/2022-09-01/i)
  const priceEl = screen.getByText(/GBP\s+-100.01/i)

  expect(dateEl).toBeInTheDocument()
  expect(priceEl).toBeInTheDocument()
})
