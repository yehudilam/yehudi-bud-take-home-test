import * as React from 'react'
import { act, render, screen, waitFor } from '@testing-library/react'
import '@testing-library/jest-dom'
import App from '../App'
import { mockFetch } from './mock/mockFetch'

describe('App tests', () => {
  beforeEach(() => {
    global.fetch = jest.fn().mockImplementation(mockFetch as jest.Mock)
  })

  afterEach(() => {
    jest.restoreAllMocks()
  })

  test('Loading page', async () => {
    act(() => {
      render(<App />)
    })

    const loading = screen.getByText(/loading/i)

    expect(loading).toBeInTheDocument()

    await waitFor(() => {
      expect(screen.queryByText(/loading/i)).toBeNull()
    })

    // balance, and 3 items in transactions
    expect(screen.queryAllByText(/GBP/i)).toHaveLength(4)
  })
})
