import { MockApiResponse } from '../../interfaces'

const json = {
  id: '0eb7acfd6fa3449676947c9521311cfce618bf9129ac5ac07ba30c76843e0f65fddb',
  provider: {
    title: 'Monzo',
    account_number: '12345678',
    sort_code: '12-34-56',
    description: 'Current Account'
  },
  balance: {
    amount: 1250.32,
    currency_iso: 'GBP'
  },
  transactions: [
    {
      id: 'dbad80ded0d2d3419749a8dd391a61bc1b5970bdfffc27f254568ec86e5c0fa06bcc',
      date: '2018-06-22',
      description: 'Max Mustermann',
      category_title: 'Income',
      amount: {
        value: 510.55,
        currency_iso: 'GBP'
      }
    },
    {
      id: '7580594e47b09ce6d22addaccb2fcf6c4f6297e616c0cecbf8acc902e1813a161816',
      date: '2018-06-29',
      description: 'British Gas',
      category_title: 'Bills',
      amount: {
        value: -48.75,
        currency_iso: 'GBP'
      }
    },
    {
      id: '4c4376c987b1f60c925466945a945ada89b1a6ae29e0bf99aa0bf997395ab5bdb7d3',
      date: '2018-07-04',
      description: 'Costa Coffee',
      category_title: 'Eating Out',
      amount: {
        value: -3.75,
        currency_iso: 'GBP'
      }
    },
    {
      id: '6616af510e42704b172260140e98d7405617a5001b37b0f7fa7abadaba44860990b9',
      date: '2018-07-03',
      description: 'Ebay',
      category_title: 'Shopping',
      amount: {
        value: -120.29,
        currency_iso: 'GBP'
      }
    }
  ]
}

export function mockFetch (): {json: () => MockApiResponse} {
  return {
    json: () => json
  }
}
