import React from 'react'
import styled from 'styled-components'

export const Date = styled.p`
  flex: 0 0 10rem;
`
export const Description = styled.p`
  text-align: center;
  flex: 1 0 12rem;
`
export const Category = styled.p`
  text-align: center;
  flex: 1 0 12rem;
`
export const Amount = styled.p`
  flex: 0 0 10rem;
  text-align: right;
`

export const TransactionRow = styled.article`
  display: flex;
  justify-content: space-between;
  min-width: 44rem;

  & p {
    padding: 2px 0;
  }
`

export const TransactionHeaderRow = styled(TransactionRow)`
  font-weight: bold;
  border-bottom: 1px solid grey;
`

const TransactionHeader = (): JSX.Element => {
  return (
    <TransactionHeaderRow>
      <Date>Date</Date>
      <Description>Description</Description>
      <Category>Category</Category>
      <Amount>Amount</Amount>
    </TransactionHeaderRow>
  )
}

export default TransactionHeader
