import React from 'react'
import { Transaction } from '../interfaces'
import { Amount, Category, Date, Description, TransactionRow } from './TransactionHeader'
import styled from 'styled-components'

interface TransactionItemProps {
  transaction: Transaction
}

// hover
const TransactionBodyRow = styled(TransactionRow)`
  &:hover {
    background: #eee;
  }
`

const TransactionItem = ({
  transaction
}: TransactionItemProps): JSX.Element => {
  return (
    <TransactionBodyRow>
      <Date>{transaction.date ?? ''}</Date>
      <Description>{transaction.description ?? ''}</Description>
      <Category>{transaction.category_title ?? ''}</Category>
      <Amount>{transaction.amount.currency_iso ?? ''} {transaction.amount.value ?? ''}</Amount>
    </TransactionBodyRow>
  )
}

export default TransactionItem
