import { DATA_URL } from '../constants'
import { MockApiResponse } from '../interfaces'

export const mockData = async (): Promise<MockApiResponse> => {
  const response = await fetch(DATA_URL)
  return (await response.json()) as MockApiResponse
}
